FROM python:3.8.12-slim-bullseye

MAINTAINER wangkun23 <845885222@qq.com>

WORKDIR /rally
RUN mkdir -p /rally/.rally
RUN apt-get -y update && \
    apt-get install -y curl git gcc pbzip2 pigz && \
    apt-get -y upgrade && \
    rm -rf /var/lib/apt/lists/*

ENV RALLY_RUNNING_IN_DOCKER True

RUN pip3 install --upgrade pip setuptools wheel
RUN pip3 install esrally==2.3.0
ENV PATH=/rally/venv/bin:$PATH


VOLUME ["/root/.rally"]

ENTRYPOINT ["/bin/bash"]
